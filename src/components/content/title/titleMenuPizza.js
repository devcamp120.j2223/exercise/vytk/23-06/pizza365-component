import { Component } from "react";

class TitleMenuPizza extends Component {
    render() {
        return (
            <div className="col-sm-12 text-center p-4 mt-4 text-orange">
                <h2><b className="p-1 border-bottom-orange">Chọn size pizza</b></h2>
                <p><span className="p-2">Chọn combo pizza phù hợp với nhu cầu của bạn.</span></p>
            </div>
        )
    }
}
export default TitleMenuPizza;