import { Component } from "react";

class TitleHome extends Component {
    render() {
        return (
            <div className="col-sm-4 text-orange contentHeader">
            <div className="mt-4">
              <h2><b>Pizza 365</b></h2>
              <p className="p-truly-italian">Truly italian !</p>
            </div>
          </div>
        )
    }
}
export default TitleHome;