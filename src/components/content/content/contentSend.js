import { Component } from "react";

class ContentSend extends Component {
    render() {
        return (
            <div className="col-sm-12 p-2 jumbotron">
            <div className="row">
              <div className="col-sm-12">
                <div className="from-group">
                  <label htmlFor="fullname">Tên</label>
                  <input type="text" className="form-control" id="inp-fullname" placeholder="Nhập tên" />
                </div>
                <div className="from-group mt-3">
                  <label htmlFor="email">Email</label>
                  <input type="text" className="form-control" id="inp-email" placeholder="Nhập email" />
                </div>
                <div className="from-group mt-3">
                  <label htmlFor="dien-thoai">Số điện thoại</label>
                  <input type="text" className="form-control" id="inp-dien-thoai" placeholder="Nhập số điện thoại" />
                </div>
                <div className="from-group mt-3">
                  <label htmlFor="dia-chi">Địa chỉ</label>
                  <input type="text" className="form-control" id="inp-dia-chi" placeholder="Địa chỉ" />
                </div>
                <div className="from-group mt-3">
                  <label htmlFor="voucher">Mã giảm giá</label>
                  <input type="text" className="form-control" id="inp-voucher" placeholder="Nhập mã giảm giá" />
                </div>
                <div className="from-group mt-3">
                  <label htmlFor="message">Lời nhắn</label>
                  <input type="text" className="form-control" id="inp-message" placeholder="Nhập lời nhắn" />
                </div>
                <button type="button" className="btn bg-orange form-control mt-3" id="btn-send">Gửi</button>
              </div>
            </div>
          </div>
        )
    }
}
export default ContentSend;