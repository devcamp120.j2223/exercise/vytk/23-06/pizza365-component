import { Component } from "react";

class ContentMenuPizza extends Component {
    render() {
        return (
            <div className="col-sm-12">
            <div className="row text-center">

              <div className="col-sm-4" style={{paddingLeft:"10px"}}>
                <div className="card" style={{width:"100%"}}>
                  <div className="card-header bg-orange text-center">
                    <h3>S (small)</h3>
                  </div>
                  <div className="card-body text-center">
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item"> Đường kính:&nbsp;<b>20cm</b></li>
                      <li className="list-group-item"> Sườn nướng:&nbsp;<b>2</b></li>
                      <li className="list-group-item"> Salad:&nbsp;<b>200g</b></li>
                      <li className="list-group-item"> Nước ngọt:&nbsp;<b>2</b></li>
                      <li className="list-group-item">
                        <h1><b>150.000</b></h1>
                        <p>VNĐ</p>
                      </li>
                    </ul>
                  </div>
                  <div className="card-footer text-center">
                    <button className="btn bg-orange form-control" id="btn-small">Chọn</button>
                  </div>
                </div>
              </div>

              <div className="col-sm-4" style={{paddingLeft:"10px"}}>
                <div className="card" style={{width:"100%"}}>
                  <div className="card-header bg-warning text-center">
                    <h3>M (medium)</h3>
                  </div>
                  <div className="card-body text-center">
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item"> Đường kính:&nbsp;<b>25cm</b></li>
                      <li className="list-group-item"> Sườn nướng:&nbsp;<b>4</b></li>
                      <li className="list-group-item"> Salad:&nbsp;<b>300g</b></li>
                      <li className="list-group-item"> Nước ngọt:&nbsp;<b>3</b></li>
                      <li className="list-group-item">
                        <h1><b>200.000</b></h1>
                        <p>VNĐ</p>
                      </li>
                    </ul>
                  </div>
                  <div className="card-footer text-center">
                    <button className="btn bg-orange form-control" id="btn-medium">Chọn</button>
                  </div>
                </div>
              </div>

              <div className="col-sm-4" style={{paddingLeft:"10px"}}>
                <div className="card" style={{width:"100%"}}>
                  <div className="card-header bg-orange text-center">
                    <h3>L (large)</h3>
                  </div>
                  <div className="card-body text-center">
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item"> Đường kính:&nbsp;<b>30cm</b></li>
                      <li className="list-group-item"> Sườn nướng:&nbsp;<b>8</b></li>
                      <li className="list-group-item"> Salad:&nbsp;<b>500g</b></li>
                      <li className="list-group-item"> Nước ngọt:&nbsp;<b>4</b></li>
                      <li className="list-group-item">
                        <h1><b>250.000</b></h1>
                        <p>VNĐ</p>
                      </li>
                    </ul>
                  </div>
                  <div className="card-footer text-center">
                    <button className="btn bg-orange form-control" id="btn-large">Chọn</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )
    }
}
export default ContentMenuPizza;