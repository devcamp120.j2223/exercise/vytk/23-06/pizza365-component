import { Component } from "react";
import seafood from '../../../assets/images/seafood.jpg';
import bacon from '../../../assets/images/bacon.jpg';
import hawaiian from '../../../assets/images/hawaiian.jpg';
class ContentChoosPizza extends Component {
    render() {
        return (
            <div className="col-sm-12">
            <div className="row">
              <div className="col-sm-4">
                <div className="card w-100">
                  <img src={seafood} className="card-img-top" alt='seafood' />
                  <div className="card-body">
                    <h4>OCEAN MANIA</h4>
                    <p>PIZZA HẢI SẢN XỐT MYONNNAISE</p>
                    <p>Xốt Cà chua, Phô mai Mozzarella, Tôm, Mực, Thanh Cua, Hành Tây.</p>
                    <p><button className="btn bg-orange form-control" id="btn-hai-san">Chọn</button>
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-sm-4">
                <div className="card w-100">
                  <img src={hawaiian} className="card-img-top" alt='haiwaiian' />
                  <div className="card-body">
                    <h4>HAWAIIAN</h4>
                    <p>PIZZA DĂM BÔNG DỨA KIỂU HAWAII</p>
                    <p>Xốt Cà Chua, Phô Mai Mozzarella, Thịt Dăm Bông, Thơm.</p>
                    <p><button className="btn bg-orange form-control" id="btn-hawai">Chọn</button>
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-sm-4">
                <div className="card w-100">
                  <img src={bacon} className="card-img-top" alt='bacon'/>
                  <div className="card-body">
                    <h4>CHEESY CHICKEN BACON</h4>
                    <p>PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                    <p>Xốt Phô Mai, Thịt Gà, Thịt Heo Muối, Phô Mai Mozzarella, Cà Chua.</p>
                    <p><button className="btn bg-orange form-control" id="btn-thit-hun-khoi">Chọn</button></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )
    }
}
export default ContentChoosPizza;