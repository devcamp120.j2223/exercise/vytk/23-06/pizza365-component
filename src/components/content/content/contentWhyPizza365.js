import { Component } from "react";

class ContentWhyPizza365 extends Component {
    render() {
        return (
            <div className="col-sm-12">
            <div className="row">
              <div className="col-sm-3 p-4 bg-lightgoldenrodyellow border border-warning">
                <h3 className="p-2">Đa dạng</h3>
                <p className="p-2">Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện nay.</p>
              </div>
              <div className="col-sm-3 p-4 bg-yellow border border-warning">
                <h3 className="p-2">Chất lượng</h3>
                <p className="p-2">Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an
                  toàn thực phẩm.</p>
              </div>
              <div className="col-sm-3 p-4 bg-lightsalmon border border-warning">
                <h3 className="p-2">Hương vị</h3>
                <p className="p-2">Đảm bảo hương vị ngon độc lạ mà bạn chỉ có thể trải nghiệm từ Pizza 365.
                </p>
              </div>
              <div className="col-sm-3 p-4 bg-orange border border-warning">
                <h3 className="p-2">Dịch vụ</h3>
                <p className="p-2">Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh chất lượng,
                  tân tiến.</p>
              </div>
            </div>
          </div>
        )
    }
}
export default ContentWhyPizza365;