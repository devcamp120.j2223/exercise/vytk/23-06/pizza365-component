
import { Component } from "react";
import TitleHome from "./title/titleHome";
import SlideHome from "./content/SlideHome";
import TitleWhyPizza365 from "./title/titleWhyPizza365";
import ContentWhyPizza365 from "./content/contentWhyPizza365";
import TitleMenuPizza from "./title/titleMenuPizza";
import ContentMenuPizza from "./content/contentMenuPizza";
import TitleChoosePizza from "./title/titleChoosePizza";
import ContentChoosPizza from "./content/contentChoosePizza";
import TitleDrink from "./title/titleDrink";
import ContentDrink from "./content/contentDrink";
import TitleSend from "./title/titleSend";
import ContentSend from "./content/contentSend";
class ContentComponent extends Component {
    render() {
        return (
            <div className="container mt">
            <div className="row" id="container1">
              <div className="col-sm-12">
                <div className="row">
                  {/* <!-- Title row home --> */}
                    <TitleHome/>
                  {/* <!-- Slide row home --> */}
                  <div className="col-sm-12">
                    <SlideHome/>
                  </div>
                  {/* <!-- Title Tại sao lại pizza 365 --> */}
                    <TitleWhyPizza365/>
                  {/* <!-- Content Tại sao lại pizza 365 --> */}
                    <ContentWhyPizza365/>
                </div>
    
                <div id="plans" className="row">
                  {/* <!-- Title Menu pizza --> */}
                    <TitleMenuPizza/>
                  {/* <!-- Content Menu pizza --> */}
                    <ContentMenuPizza/>
                </div>
    
                <div id="about" className="row">
                  {/* <!-- Title Chọn loại Pizza --> */}
                    <TitleChoosePizza/>
    
                  {/* <!-- Content Chọn loại Pizza --> */}
                    <ContentChoosPizza/>
                </div>
    
                <div id="plans" className="row">
                  {/* <!-- Title Đồ Uống --> */}
                    <TitleDrink/>
                  {/* <!-- Content Đồ Uống --> */}
                    <ContentDrink/>
                </div>
                <div id="contact" className="row">
                  {/* <!-- Title Gửi đơn hàng --> */}
                    <TitleSend/>
    
                  {/* <!-- Content Gửi đơn hàng --> */}
                    <ContentSend/>
                </div>
              </div>
            </div>
          </div>
        )
    }
}
export default ContentComponent;