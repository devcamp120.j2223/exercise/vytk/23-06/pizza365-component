import { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faFacebookSquare, faInstagram, faSnapchat, faPinterestP, faTwitter, faLinkedinIn } from "@fortawesome/free-brands-svg-icons"
import {faArrowUp} from "@fortawesome/free-solid-svg-icons"
class Footer extends Component {
    render() {
        return (
            <div className="container-fluid bg-orange p-5">
            <div className="row text-center">
              <div className="col-sm-12">
                <h4 className="m-2">Footer</h4>
                <a href="#" className="btn btn-dark m-3"><FontAwesomeIcon icon={faArrowUp} /> To the top</a>
                <div className="m-2">
                  <FontAwesomeIcon icon={faFacebookSquare} />&nbsp;
                  <FontAwesomeIcon icon={faInstagram} />&nbsp;
                  <FontAwesomeIcon icon={faSnapchat} />&nbsp;
                  <FontAwesomeIcon icon={faPinterestP} />&nbsp;
                  <FontAwesomeIcon icon={faTwitter} />&nbsp;
                  <FontAwesomeIcon icon={faLinkedinIn} />&nbsp;
                  <p>Powered by DEVCAMP</p>
                </div>
              </div>
            </div>
          </div>
        )
    }
}
export default Footer;