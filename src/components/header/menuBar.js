import { Component } from "react";

class MenuBar extends Component {
    render() {
        return (
            <div className="container">
                <div className="navbar">
                    <div className="col-sm-12">
                    <nav className="navbar fixed-top navbar-expand-lg navbar-light w-100 bg-orange">
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav nav-fill w-100">
                            <li className="nav-item active">
                            <a className="nav-link" href="#container1">Trang chủ</a>
                            </li>
                            <li className="nav-item">
                            <a className="nav-link" href="#plans">Combo</a>
                            </li>
                            <li className="nav-item">
                            <a className="nav-link" href="#about">Loại pizza</a>
                            </li>
                            <li className="nav-item">
                            <a className="nav-link" href="#" id="a-orderid">Gửi đơn hàng</a>
                            </li>
                        </ul>
                        </div>
                    </nav>
                    </div>
                </div>
            </div>
        )
    }
}
export default MenuBar;