
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
//Import Components
import MenuBar from './components/header/menuBar';
import ContentComponent from './components/content/contentComponent';
import Footer from './components/footer/footer';

function App() {
  return (
    <div>
      {/* <!-- Component Header MenuBar --> */}
        <MenuBar/>
      {/* <!-- Content --> */}
        <ContentComponent/>
      {/* <!-- Footer --> */}
        <Footer/>
    </div>
  );
}

export default App;
